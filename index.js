//Вот тебе массив 
let arr = [1,2,3,4,5,6,7]
// А вот простой цикл по этому массиву
// arr.length - Это длина массива. Массив в JS это объект, а у объекта как изветсно есть свойства. length - это свойство массива для пустого
// массива оно равно 0 . В данно случае 7
for( let i = 0; i < arr.length; i++ ) {
    console.log( arr[i] );
}
//1
//2
//3
//4
//5
//6
//7
//В данном случае мы выводим содержимое элементов массива где i - это индекс каждого элемента. Я начал с начала - с нулевого индекса!
    
